// Listen on a specific host via the HOST environment variable
var host = '0.0.0.0';
// Listen on a specific port via the PORT environment variable
var port = 8000;

var cors_proxy = require('cors-anywhere');
cors_proxy.createServer({
    originWhitelist: [],
}).listen(port, host, function() {
    console.log('Running CORS-Proxy on ' + host + ':' + port);
});