# Ibern’s solution for custom weather-widget

## Requirements

- Angular CLI: 10.0.8
- Angular: 10.0.14
- Node: 12.13.1

To start the project;
npm run-script start

The project will start angular at port 4200 (4201 if npm run-script dev instead) project and the little api proxy-like it needs to redirect get-requests and sort CORS problems when requesting straight from browsers at port 8000.

## Widget:
I wanted to do something clear, simple and functional.
On load, the widget will retrieve the last location setted up. If it can’t, will apply Trondheim’s data instead.
The location can be changed by clicking the settings button on the right (gear image). This will open a modal with an input text field and a “search” button that triggers the location search. 
If there’s more than one location for this search, the eligible locations will appear below the text field. The results can be selected by clicking on them, then the modal will close automatically loading the desired data.

### TODO:
- ~~Favourite list~~ Done
- ~~Change background colour depending on current time (clear blue during the day, orange~ish at evening, dark blue at night)~~ Done
- Timezone responsiveness: I’ve noticed that the forecast’s results are given in GMT; I’ve completely missed that detail last week. Todo from further improvements.

## Added dependencies:

### Ng-bootstrap (9.0.2)
- I’ve used a couple of modules like the NgModal, and NgForms.

### Bootstrap (4.6.0)
- Something nearly essential to have a component like this; at least for it’s grid-responsive methods. Also, Ng-bootstrap relies on bootstrap’s css

### Font-Awesome (4.7.0)
- I was not going to include it, because I had only to use a single icon so far (the setting-gear one), but after including the Favourite-list feature, It was handy to have it installed.

### Ngx-cookie-service (11.0.2)
- To store user’s data, such as favourites, and settings (not applied at this point.)