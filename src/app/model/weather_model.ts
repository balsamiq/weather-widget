export interface Weather {
    symbol_code: string;
    air_temperature: string;
    time: string;
  }
// object.time
// object.data.instant.details.air_temperature
// object.data.next_1_hours.summary.symbol_code