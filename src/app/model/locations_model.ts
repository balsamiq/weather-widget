export interface Locations {
    name: string;
    countryName: string;
    lat: string;
    lon: string;
  }

export interface UsedLocations{
  name: string;
  lat:string;
  lon:string;
}

export interface CurrentLocationFav{
  isFavourite: boolean;
  class: string;
}