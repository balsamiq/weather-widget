import { Component, OnInit } from '@angular/core';
import { ApiService, UtilsService } from '../services';
import { Locations, UsedLocations, CurrentLocationFav } from '../model/locations_model';
import { Weather } from '../model/weather_model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'


@Component({
  selector: 'app-weather-widget',
  templateUrl: './weather-widget.component.html',
  styleUrls: ['./weather-widget.component.scss']
})
export class WeatherWidgetComponent implements OnInit {

  
  locations: Array<Locations> //Array of locations available -> result from search by name
  todaysWeather: Array<Weather> //Array of weather divided by intervals as requested.
  intervals: Array<string> = ["00:00:00Z", "06:00:00Z", "12:00:00Z", "18:00:00Z"];
  locationSearch: string;
  weekDay: string;
  backgroundImage: string;
  favouriteList: Array<UsedLocations>;
  currentLoc: UsedLocations;
  isCurrentFav:CurrentLocationFav;
  currentDate: string;
  loader: boolean;

  constructor(
    private apiService: ApiService,
    private utilsService: UtilsService,
    private modalService: NgbModal
  ) {
    this.loader = false;
  }

  ngOnInit(): void {
    this.loader = true;
    this.backgroundImage = "";
    this.locations = [];
    this.todaysWeather = [];
    this.setEnvBackground();
    this.loadLastSetting();
    this.favouriteList = this.utilsService.recoverFav();
    this.checkIsCurrentFavs();
    this.locationSearch = this.currentLoc.name;
    this.weekDay = this.utilsService.getDayOfWeek();
  }

  //Modal related resources
  openSearchModal(longContent) {
    this.modalService.open(longContent, { scrollable: true, size: 'lg' });
  }
  closeSearchModal() {
    this.modalService.dismissAll();
  }

  //Load last data available; This initialize the entire widget.
  loadLastSetting() {
    this.currentLoc = this.getLastLocation();
    this.locations = [];
    //Loading last known setting. (or Trondheim by default);
    this.apiService.getWeatherByCoords(this.currentLoc.lat, this.currentLoc.lon).subscribe(response => {
      if (response) {
        //Getting 'Now'
        let parsed_symbol = `background-image: url('/assets/icons/${response.properties.timeseries[0].data.next_1_hours.summary.symbol_code}.svg');`
        const weatherSnap: Weather = {
          symbol_code: parsed_symbol,
          air_temperature: response.properties.timeseries[0].data.instant.details.air_temperature,
          time: response.properties.meta.updated_at
        }
        this.todaysWeather.push(weatherSnap);
        response.properties.timeseries.forEach(item => {
          // The given weather API is already serving the data in order, so I can take just the following 3 matches.
          if (this.todaysWeather.length < 4) {
            let hour = item.time.substr(item.time.length - 9);
            if (this.intervals.includes(hour)) {
              let parsed_symbol = `background-image: url('/assets/icons/${item.data.next_1_hours.summary.symbol_code}.svg');`
              const weatherSnap: Weather = {
                symbol_code: parsed_symbol,
                air_temperature: item.data.instant.details.air_temperature,
                time: this.utilsService.getTrimmedHours(item.time),
              }
              this.todaysWeather.push(weatherSnap);
              this.locations = []
              this.loader = false;
            }
          }
        });
      } else {
        console.log("Couldn't retreive data from API");
        this.loader = false;
      }
    }, error => {
      console.log(error);
      this.loader = false;
    });
    this.checkIsCurrentFavs();
  }

  getLocationsByName(name: string) {
    this.locations = [];
    this.apiService.getLocations(name).subscribe(response => {
      if (response) {
        response.geonames.forEach(geoname => {
          const loc: Locations = {
            name: geoname.name || "",
            countryName: geoname.countryName || "",
            lat: geoname.lat || "0",
            lon: geoname.lng || "0",
          }
          this.locations.push(loc);
        });
        if (this.locations.length == 1) {
          this.getWeatherFromCoords(this.locations[0].lat, this.locations[0].lon, this.locations[0].name)
        }
      }
    }, error => {
      console.log(error);
    });
  }

  getWeatherFromCoords(lat: string, lon: string, name?: string) {
    this.loader = true;
    this.todaysWeather = [];
    if (name) {
      //Save the current location as a last location in LocalStorage.
      this.setLastLocation(name, lat, lon);
    }
    this.apiService.getWeatherByCoords(lat, lon).subscribe(response => {
      if (response) {
        //Getting 'Now'
        let parsed_symbol = `background-image: url('/assets/icons/${response.properties.timeseries[0].data.next_1_hours.summary.symbol_code}.svg');`
        const weatherSnap: Weather = {
          symbol_code: parsed_symbol,
          air_temperature: response.properties.timeseries[0].data.instant.details.air_temperature,
          time: response.properties.meta.updated_at
        }
        this.todaysWeather.push(weatherSnap);
        response.properties.timeseries.forEach(item => {
          // The given weather API is already serving the data in order, so I can take just the following 3 matches.
          if (this.todaysWeather.length < 4) {
            let hour = item.time.substr(item.time.length - 9);
            if (this.intervals.includes(hour)) {
              let parsed_symbol = `background-image: url('/assets/icons/${item.data.next_1_hours.summary.symbol_code}.svg');`
              const weatherSnap: Weather = {
                symbol_code: parsed_symbol,
                air_temperature: item.data.instant.details.air_temperature,
                time: this.utilsService.getTrimmedHours(item.time),
              }
              this.todaysWeather.push(weatherSnap);
            }
          }
        });
        this.locations = [];
        this.loader = false;
        this.closeSearchModal();
      }
    }, error => {
      console.log(error);
      this.loader = false;
    });
    this.checkIsCurrentFavs();
  }

  setEnvBackground() {
    //The background image will change depending on the current hour.
    const currentHour = new Date().getHours();
    let daySegment = '';
    if (currentHour > 17 && currentHour <= 20) {
      daySegment = 'sky_evening';
    } else if (currentHour > 20 || currentHour < 7) {
      daySegment = 'sky_night'
    } else {
      daySegment = 'sky_day'
    }
    this.backgroundImage = `background-image: url('/assets/images/${daySegment}.png')`
  }

  getLastLocation(): UsedLocations {
    // First time around? default location will be Trondheim, 63.446827, 10.421906. Ideally
    // I would use an auto-location component to guess the user's location, but I couldn't find any api-key for the test
    let res: UsedLocations = {
      name: localStorage.getItem('lastLocation.name') || "Trondheim",
      lat: localStorage.getItem('lastLocation.lat') || "63.446827",
      lon: localStorage.getItem('lastLocation.lon') || "10.421906"
    }
    return res;
  };

  setLastLocation(name: string, lat: string, lon: string) {
    //It just persists the current search, so it must remain saved for next session.
    localStorage.setItem('lastLocation.name', name);
    localStorage.setItem('lastLocation.lat', lat);
    localStorage.setItem('lastLocation.lon', lon);
    this.currentLoc = this.getLastLocation();
  }

  toggleFavourite(name, lat, lon) {
    // Empty input field? do nothing.
    if (name && lat && lon) {
      const locationToToggle: UsedLocations = {
        name: name,
        lat: lat,
        lon: lon,
      };
      this.favouriteList = this.utilsService.toggleFav(this.favouriteList, locationToToggle);
      this.checkIsCurrentFavs();
    }
  }

  checkIsCurrentFavs(){
    //Check if the selected location is a favourite.
    this.isCurrentFav = this.utilsService.isFavourite(this.currentLoc)
  }
}
