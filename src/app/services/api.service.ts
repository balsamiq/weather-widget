import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpService } from './http.service';

@Injectable()
export class ApiService {

    private weatherApi = `https://api.met.no/weatherapi/locationforecast/2.0/complete?`;
    private geoUser = `eggstech`;
    private geonamesApi = `http://api.geonames.org/searchJSON?formatted=true&countryBias=no&maxRows=10&lang=en&orderBy=population&cities=cities500&`;

    constructor(
        private httpService: HttpService,
        private router: Router,
    ) { }

    getLocations(tropoName: string): Observable<any> {
        const url = `${this.geonamesApi}name_startsWith=${tropoName}&username=${this.geoUser}`;
        return this.httpService.doGet(url);
    }

    getWeatherByCoords(lat: string, lon: string): Observable<any> {
        const url = `${this.weatherApi}lat=${lat}&lon=${lon}`;
        return this.httpService.doGet(url);
    }
}
