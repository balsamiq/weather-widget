import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class HttpService {

    private corsBridgePrefix = `http://localhost:8000/`

    constructor(
        private http: HttpClient,
    ) { }

    doGet(url: string, token?: string): Observable<any> {
        let bridgedUrl = this.corsBridgePrefix + url
        // console.log('Do Get:' + bridgedUrl);
        // let bridgedUrl = url;
        let headers: HttpHeaders = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        // Sorry Pål, this didn't worked out in my case:
        // headers = headers.append('User-Agent', 'EGGS-TECH');
        // It seems like my browsers override this header's property.
        // Here's a possible explanation why -> https://stackoverflow.com/questions/39999687/it-is-possible-to-set-the-user-agent-header-used-by-http-in-angular2
        const options = { headers: headers, withCredentials: false };
        return this.http.get(bridgedUrl, options)
    }
}
