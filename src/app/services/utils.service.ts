import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { UsedLocations, CurrentLocationFav } from '../model/locations_model';

@Injectable()
export class UtilsService {

    constructor(
        private cookieService: CookieService
    ) { }

    getDayOfWeek() {
        const dayOfWeek = new Date().getDay();
        return isNaN(dayOfWeek) ? null :
            ['Søndag', 'Mandag', 'Tirsdag', 'Onsdag', 'Torsdag', 'Fredag', 'Lørdag'][dayOfWeek];
    }

    getTrimmedHours(zuluDate): string {
        let res = zuluDate.substr(zuluDate.length - 9);
        return res.substring(0, res.length - 4);
    }

    // Fabourite system:
    // I've used cookies to store the favourite-related data in the user's frontend.
    // Recover last data saved from other sessions (if they persists in cookies, ofc...)
    recoverFav(): Array<UsedLocations> {
        // There is too little data to store, so I've chosen to store it in the same variable, using '/' to divide objects, and '_' for properties.
        //It's a matter of splitting the desired data, and return it as an object's array properly formatted
        let rawHystory = this.cookieService.get("eggs_widget_favs");
        let result: Array<UsedLocations>;
        result = [];
        if (rawHystory && rawHystory != "undefined") {
            rawHystory.split("/").forEach(location => {
                if (location) {
                    let locSplitted = location.split('_');
                    let newLoc: UsedLocations = {
                        name: locSplitted[0],
                        lat: locSplitted[1],
                        lon: locSplitted[2]
                    }
                    result.push(newLoc)
                }
            });
        }
        return result;
    }

    applyFav(favList: Array<UsedLocations>) {
        // Store the given array, parsed into string
        let result: string;
        favList.forEach(location => {
            if (!result) result = "";
            result = result + location.name + "_" + location.lat + "_" + location.lon + "/";
        });
        this.cookieService.set("eggs_widget_favs", result);
    }

    toggleFav(favList: Array<UsedLocations>, newFav: UsedLocations): Array<UsedLocations> {
        let index: number;
        //Check if there's any item in the list.
        if (favList) {
            //Looks if the item is already in the Fav. list.
            for (var i = 0; i < favList.length; i++) {
                if (favList[i].name == newFav.name) {
                    index = i;
                    break;
                }
            }
            if (index > -1) {
                //If it is, remove it and apply changes.
                favList.splice(index, 1);
                this.applyFav(favList);
                return favList;
            }
            else {
                //If it is not, but the list has already 5 items, remove the last one.
                if (favList.length == 5) favList.shift();
            }
        } else {
            favList = []
        }
        //If it is not in the list, the new item is added and the changes applied.
        favList.push(newFav);
        this.applyFav(favList);
        return favList;
    }

    isFavourite(location: UsedLocations): CurrentLocationFav {
        let response: CurrentLocationFav = {
            class: "fa fa-star-o",
            isFavourite: false
        };
        let favouriteList = this.recoverFav();
        if (favouriteList) {
            for (var i = 0; i < favouriteList.length; i++) {
                if (favouriteList[i].name == location.name) {
                    response = {
                        class: "fa fa-star",
                        isFavourite: false
                    };
                    break;
                }
            }
        }
        return response;
    }
}